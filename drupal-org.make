; Makefile for UA Quickstart distribution
core = 7.x
api = 2

; =====================================
; UA Modules
; =====================================

projects[ua_block_types][type] = module
projects[ua_block_types][subdir] = custom
projects[ua_block_types][download][type] = git
projects[ua_block_types][download][branch] = 7.x-1.x
projects[ua_block_types][download][revision] = 1a88896
projects[ua_block_types][download][url] = https://bitbucket.org/ua_drupal/ua_block_types.git

projects[ua_cas][type] = module
projects[ua_cas][subdir] = custom
projects[ua_cas][download][type] = git
projects[ua_cas][download][branch] = 7.x-1.x
projects[ua_cas][download][revision] = 434ecb1
projects[ua_cas][download][url] = https://bitbucket.org/ua_drupal/ua_cas.git

projects[ua_core][type] = module
projects[ua_core][subdir] = custom
projects[ua_core][download][type] = git
projects[ua_core][download][branch] = 7.x-1.x
projects[ua_core][download][revision] = a6332ab
projects[ua_core][download][url] = https://bitbucket.org/ua_drupal/ua_core.git

projects[ua_demo][type] = module
projects[ua_demo][subdir] = custom
projects[ua_demo][download][type] = git
projects[ua_demo][download][branch] = 7.x-1.x
projects[ua_demo][download][revision] = 473f89a
projects[ua_demo][download][url] = https://bitbucket.org/ua_drupal/ua_demo.git

projects[ua_event][type] = module
projects[ua_event][subdir] = custom
projects[ua_event][download][type] = git
projects[ua_event][download][branch] = 7.x-1.x
projects[ua_event][download][revision] = e3ef060
projects[ua_event][download][url] = https://bitbucket.org/ua_drupal/ua_event.git

projects[ua_featured_content][type] = module
projects[ua_featured_content][subdir] = custom
projects[ua_featured_content][download][type] = git
projects[ua_featured_content][download][branch] = 7.x-1.x
projects[ua_featured_content][download][revision] = 7698816
projects[ua_featured_content][download][url] = https://bitbucket.org/ua_drupal/ua_featured_content.git

projects[ua_google_tag][type] = module
projects[ua_google_tag][subdir] = custom
projects[ua_google_tag][download][type] = git
projects[ua_google_tag][download][branch] = 7.x-1.x
projects[ua_google_tag][download][revision] = 21a223a
projects[ua_google_tag][download][url] = https://bitbucket.org/ua_drupal/ua_google_tag.git

projects[ua_navigation][type] = module
projects[ua_navigation][subdir] = custom
projects[ua_navigation][download][type] = git
projects[ua_navigation][download][branch] = 7.x-1.x
projects[ua_navigation][download][revision] = 56d028a
projects[ua_navigation][download][url] = https://bitbucket.org/ua_drupal/ua_navigation.git

projects[ua_news][type] = module
projects[ua_news][subdir] = custom
projects[ua_news][download][type] = git
projects[ua_news][download][branch] = 7.x-1.x
projects[ua_news][download][revision] = e6b3cc4
projects[ua_news][download][url] = https://bitbucket.org/ua_drupal/ua_news.git

projects[ua_page][type] = module
projects[ua_page][subdir] = custom
projects[ua_page][download][type] = git
projects[ua_page][download][branch] = 7.x-1.x
projects[ua_page][download][revision] = e7ea682
projects[ua_page][download][url] = https://bitbucket.org/ua_drupal/ua_page.git

projects[ua_person][type] = module
projects[ua_person][subdir] = custom
projects[ua_person][download][type] = git
projects[ua_person][download][branch] = 7.x-1.x
projects[ua_person][download][revision] = 83ad243
projects[ua_person][download][url] = https://bitbucket.org/ua_drupal/ua_person.git

projects[ua_program][type] = module
projects[ua_program][subdir] = custom
projects[ua_program][download][type] = git
projects[ua_program][download][branch] = 7.x-1.x
projects[ua_program][download][revision] = 3eb0354
projects[ua_program][download][url] = https://bitbucket.org/ua_drupal/ua_program.git

projects[ua_publication][type] = module
projects[ua_publication][subdir] = custom
projects[ua_publication][download][type] = git
projects[ua_publication][download][branch] = 7.x-1.x
projects[ua_publication][download][revision] = e68902f
projects[ua_publication][download][url] = https://bitbucket.org/ua_drupal/ua_publication.git

projects[ua_unit][type] = module
projects[ua_unit][subdir] = custom
projects[ua_unit][download][type] = git
projects[ua_unit][download][branch] = 7.x-1.x
projects[ua_unit][download][revision] = bc34634
projects[ua_unit][download][url] = https://bitbucket.org/ua_drupal/ua_unit.git


; =====================================
; UA Themes
; =====================================

projects[ua_zen][type] = theme
projects[ua_zen][directory_name] = ua_zen
projects[ua_zen][download][type] = git
projects[ua_zen][download][branch] = 7.x-1.x
projects[ua_zen][download][revision] = 0f0ff6e
projects[ua_zen][download][url] = https://bitbucket.org/ua_drupal/ua_zen.git
